import React, { Component } from "react";
import "./App.css";
import { CirclePicker } from "react-color";
import firebase from "firebase";

const PIXEL_SIZE = 10;

const config = {

};

firebase.initializeApp(config);

var db = firebase.firestore();

// Disable deprecated features
db.settings({
  timestampsInSnapshots: true
});

class App extends Component {
  constructor() {
    super();
    this.state = {
      pixels: [
      ],

      selectedCoordinate: {
        x: 10,
        y: 99
      }
    };
  }

  componentDidMount() {
    db.collection("pixels").onSnapshot(collection =>
      this.setState({ pixels: collection.docs.map(doc => doc.data()) })
    );
  }

  handlePixelsClicked(event) {
    if (this.state.selectedCoordinate) return;
    const coordinate = {
      x: Math.floor(event.clientX / PIXEL_SIZE),
      y: Math.floor(event.clientY / PIXEL_SIZE)
    };
    this.setState({
      selectedCoordinate: coordinate
    });
    console.log(coordinate);
  }

  handleColorPicked(color) {
    db.collection("pixels").add({
      ...this.state.selectedCoordinate,
      color: color.hex
    });

    this.setState({
      selectedCoordinate: null
    });
  }

  render() {
    return (
      <div
        id="pixels"
        onClick={event => this.handlePixelsClicked(event)}
        style={{
          width: "1000px",
          height: "1000px",
          position: "relative"
        }}
      >
        {this.state.pixels.map(pixel => (
          <div
            style={{
              position: "absolute",
              left: pixel.x * PIXEL_SIZE,
              top: pixel.y * PIXEL_SIZE,
              width: PIXEL_SIZE,
              height: PIXEL_SIZE,
              backgroundColor: pixel.color
            }}
          />
        ))}
        {this.state.selectedCoordinate && (
          <div
            style={{
              position: "absolute",
              left: this.state.selectedCoordinate.x * PIXEL_SIZE,
              top: this.state.selectedCoordinate.y * PIXEL_SIZE,
              right: 100
            }}
          >
            <CirclePicker onChange={this.handleColorPicked.bind(this)} />
          </div>
        )}
      </div>
    );
  }
}

export default App;
